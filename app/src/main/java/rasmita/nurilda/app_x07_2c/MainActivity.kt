package rasmita.nurilda.app_x07_2c

import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CursorAdapter
import android.widget.SimpleCursorAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_mhs.view.*
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnScanQR ->{
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btnGenerateQR ->{
                val barCodeEncoder = BarcodeEncoder()
                val bitmap = barCodeEncoder.encodeBitmap(edQrCode.text.toString(),
                BarcodeFormat.QR_CODE,400,400)
                imV.setImageBitmap(bitmap)
            }
            R.id.button -> {
                buil.setTitle("validasi")
                    .setMessage("apakah data sudah benar ?")
                    .setPositiveButton("Ya", insertbt)
                buil.show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        val intentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if (intentResult!=null){
            if (intentResult.contents !=null){
                edQrCode.setText(intentResult.contents)
                val strToken = StringTokenizer(edQrCode.text.toString(),";",false)
                editText.setText(strToken.nextToken())
                editText2.setText(strToken.nextToken())
                editText3.setText(strToken.nextToken())
            }else{
                Toast.makeText(this,"Dibatalkan",Toast.LENGTH_SHORT).show()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    lateinit var  intentIntegrator: IntentIntegrator
    lateinit var buil : AlertDialog.Builder
    lateinit var db : SQLiteDatabase
    lateinit var adaplis: SimpleCursorAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        intentIntegrator = IntentIntegrator(this)
        btnGenerateQR.setOnClickListener(this)
        btnScanQR.setOnClickListener(this)
        button.setOnClickListener(this)
        buil = AlertDialog.Builder(this)
        getdata()
        showtrans()

    }

    private fun showtrans() {
        var sql = "select nim as _id, nama ,prodi from mhs"
        val c: Cursor = db.rawQuery(sql, null)
        adaplis = SimpleCursorAdapter(this, R.layout.item_mhs,c, arrayOf("_id", "nama", "prodi"),
            intArrayOf(R.id.txNim, R.id.txNama, R.id.txProdi),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        listView.adapter = adaplis
    }

    private fun getdata(): SQLiteDatabase {
        db = DBOpenHelper(this).writableDatabase
        return db
    }

    fun insert(nim : String , nama : String ,prodi : String){
        val cv : ContentValues = ContentValues()
        cv.put("nim",nim)
        cv.put("nama",nama)
        cv.put("prodi",prodi)
        db.insert("mhs",null,cv)
        showtrans()
    }

    val insertbt = DialogInterface.OnClickListener { dialog, which ->
        insert(editText.text.toString(),editText2.text.toString(),editText3.text.toString())
        editText.setText("")
        editText2.setText("")
        editText3.setText("")
    }

}
